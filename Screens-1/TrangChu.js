import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import HomeScreen from '../HomeScreen';
import DetailScreen from '../DetailScreen';
const Stack = createNativeStackNavigator();

export default function TrangChu() {
  return (
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen
          name='Home'
          component={HomeScreen}
          options={{title: 'Welcome'}}
        ></Stack.Screen>
        <Stack.Screen
          name='Detail'
          component={DetailScreen}
        ></Stack.Screen>
      </Stack.Navigator>      
  );
}