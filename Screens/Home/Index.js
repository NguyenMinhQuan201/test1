import React, {Component} from 'react';
import {View, Text,Image, TouchableOpacity, StyleSheet, FlatList, Dimensions,TextInput} from 'react-native';
const width =Dimensions.get("screen").width/2-30;
import products from '../Products/products';
export default class HomeScreen extends Component {
    constructor (props) {
        super(props);
        this.navigation = props.navigation;
            this.gotoDetail = this.gotoDetail.bind(this);
            this.state = {
                data: [
                    { name: 'Đăng Nhập' },
                    { name: 'test' },
                    { name: 'canhpv' },
                ]
            }
    }
    gotoDetail(product) {
        console.log(product);
        this.navigation.navigate('Detail', {product});
    }
    render() {
        let renderButton = this.state.data.map((item, index) => (
            <TouchableOpacity key={index} style={styles.button} 
                onPress={() => this.gotoDetail(item.name)}>
                <Text style={styles.buttonText}>Go to {item.name} Screen</Text>
            </TouchableOpacity>
        ))
        // cái thanh swip thì phải
        
        const categories=['POPULAR','ORGANIC','INDOORS','SYNTHETIC'];
        
        const CategoryList=()=>{
            const [categoryIndex,setCategoryIndex]=React.useState(0);
            return (
                // Chuyền các Item của categories và xuất
                <View style={styles.categoryContainer}>
                {categories.map((item,index)=>(
                    <TouchableOpacity
                        // Set Item khi bấm vào 
                        key={index} 
                        activeOpacity={0.8}
                        onPress={()=>setCategoryIndex(index)}
                    >
                        <Text 
                        // Item hiện ra màng hình với styles
                            key={index} 
                            style={[
                                styles.categoryText,
                                categoryIndex == index && styles.CategorySelected,
                            ]}>
                            {item}
                        </Text>
                    </TouchableOpacity>
                ))}
            </View>
            );
        };
        //List Hang Hoa
        const Card=({product})=>{
            return(
                <TouchableOpacity onPress={() => this.gotoDetail(product)}>
                <View style={styles.Card}>
                    <View>                       
                        <View style={{height:120,marginTop:30,alignItems:'center',justifyContent:'center',}}>
                            <Image 
                                style={{flex:1,resizeMode:'contain'}} 
                                source={product.img}
                            />
                        </View>
                        <Text style={{fontSize:20,textAlign:'center',justifyContent:'center',fontWeight:'bold'}}>
                            {product.name}
                        </Text>
                        <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:15}}>
                            <Text style={{fontSize:15,textAlign:'left',fontWeight:'bold',marginLeft:5}}>
                                {product.price}
                            </Text>
                            <View style={{
                                height:25,
                                width:25,
                                backgroundColor:'purple',
                                borderRadius:5,
                                justifyContent:'center',
                                alignItems:'center',
                                marginRight:10,
                                
                            }}>
                                <Text style={{
                                    fontSize:18,
                                    color:'white',
                                    fontWeight:'bold',
                                    flex:1
                                }}>
                                    +
                                </Text>
                            </View>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
            );
        };
        return(           
            <View style={{backgroundColor:'purple',height:600}}>
                
                <View style={styles.header}>
                        <View style={{
                                    backgroundColor:'grey',
                                    width:300,
                                    }}>
                            <Text style={{paddingLeft:30,fontSize:20,fontWeight:'bold',color:'pink'}}>
                                    Welcome to 
                                
                            </Text>   
                            <Text style={{paddingLeft:20,fontSize:25,fontWeight:'bold',color:'pink'}}>
                                    
                                LipStickShop.com
                            </Text>        
                            
                            <View style={{flexDirection:'row'}}>
                                <View style={styles.seachContainer}>
                                    <Image                       
                                        source={require('../Home/seach.png')}
                                        style={{
                                            width:25,
                                            height:20,
                                            marginLeft:20,
                                        }}
                                    />   
                                    <TextInput placeholder="seach" style={styles.inputseach}></TextInput>  
                                </View>  
                            </View>
                        </View>

                        <View style={{backgroundColor:'grey'}} >
                            <Image                       
                                source={require('../Home/ThongBao_2.png')}
                                style={{
                                    width:50,
                                    height:50,
                                    marginLeft:30,
                                }}
                            />      
                            
                        </View>
                        
                </View>
                <CategoryList/>
                
                <FlatList 
                    numColumns={2} 
                    data={products} 
                    renderItem={({item})=><Card product={item} />}
                    columnWrapperStyle={{justifyContent:'space-between'}}
                    showsHorizontalScrollIndicator={false}
                    contentContainerStyle={{
                        marginTop:10,
                        paddingBottom:40,                        
                    }}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    Card:{
        backgroundColor:'white',
        height:225,
        width,
        marginHorizontal:2,
        borderRadius:10,
        marginBottom:20,
        marginLeft:10,
        marginRight:10,
    },
    CategorySelected:{
        color:'pink',
        paddingBottom:5,
        borderBottomWidth:2,
        borderColor:'pink',
    },
    categoryText:{
        fontSize:16,
        color:'grey',
        fontWeight:'bold',
    },
    categoryContainer:{
        backgroundColor:'white',
        flexDirection:'row',
        marginTop:30,
        marginBottom:10,
        marginLeft:10,
        marginRight:10,
        justifyContent:'space-between',
    },
    seachContainer:{
        height:40,       
        borderRadius:10,
        flex:1,
        flexDirection:'row',
        alignItems:'center',
        backgroundColor:'white',
        marginLeft:10,
    },
    inputseach:{
        fontSize:15,
        fontWeight:'bold',
        color:'green',
        backgroundColor:'white',
        borderRadius:15,
        width:230,      
    },
    seach:{
        justifyContent:'space-between',
    },
    header:{
        
        flexDirection:'row',
        justifyContent:'space-between',
        backgroundColor:'grey',
        height:110,
    },
    container: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        fontSize: 40,
        fontWeight: 'bold',
        textAlign: 'center',
        marginBottom: 30
    },
    button: {
        borderWidth: 1,
        padding: 10,
        width: 150,
        alignItems: 'center',
        marginBottom: 10
    },
    buttonText: {
        // textAlign: 'center'
    }
});